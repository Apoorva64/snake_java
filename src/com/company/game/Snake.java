package com.company.game;

import com.company.DrawHelper;
import com.company.constants.Direction;
import com.company.constants.WindowConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.company.astar.AStar;
import org.w3c.dom.Node;

public class Snake {
    private final float[] color;
    private final List<Square> squares;
    private final List<Direction> turns;
    private int[][] snake_game_map;
    private float[][] positions;
    private Direction direction;
    private Food food;
    private Square head;
    private AStar a_star_helper;
    private List<AStar.Node> path;
    private static final int[] direction_hash_codes = {
            Arrays.hashCode(new float[]{0.0f, 1.0f}),
            Arrays.hashCode(new float[]{0.0f, -1.0f}),
            Arrays.hashCode(new float[]{1.0f, 0.0f}),
            Arrays.hashCode(new float[]{-1.0f, 0.0f})
    };

    public Snake(float x, float y, float[] color) {
        this.color = color;
        this.squares = new ArrayList<>();
        this.turns = new ArrayList<>();
        this.head = new Square(x, y, Direction.RIGHT, this.color);
        this.squares.add(head);
        this.direction = Direction.RIGHT;
        update_snake_elements_position();
        float[] food_position = get_non_snake_random_position();
        this.food = new Food(food_position[0], food_position[1], new float[]{0.1f, 0.4f, 0.2f, 1});
        this.turns.add(this.direction);
        this.snake_game_map = new int[WindowConstants.TILE_NUMBER[1] * 2 + 1][WindowConstants.TILE_NUMBER[0] * 2 + 1];

    }

    public void update_snake_elements_position() {
        this.positions = new float[this.squares.size()][2];
        List<Square> squareList = this.squares;
        for (int i = 0, squareListSize = squareList.size(); i < squareListSize; i++) {
            Square square = squareList.get(i);
            this.positions[i] = new float[]{square.getX(), square.getY()};
        }
//        System.out.println(Arrays.deepToString(this.positions));
    }

    public void fill_snake_game_map() {
//        int[][] snakeGameMap = this.snake_game_map;
//        for (int i = 0; i < snakeGameMap.length; i++) {
//            int[] line = snakeGameMap[i];
//            for (int j = 0, lineLength = line.length; j < lineLength; j++) {
//                line[j]=0;
//
//            }
//        }
        this.snake_game_map = new int[WindowConstants.TILE_NUMBER[1] * 2 + 2][WindowConstants.TILE_NUMBER[0] * 2 + 2];
        float[][] positions = this.positions;
        for (float[] position : positions) {
            this.snake_game_map[-(int) position[1] + WindowConstants.TILE_NUMBER[1]][(int) position[0] + WindowConstants.TILE_NUMBER[0]] = 10;
        }

    }


    public float[] get_non_snake_random_position() {
        float[][] snake_positions = this.positions;
        float x = DrawHelper.random_number(-WindowConstants.TILE_NUMBER[0], WindowConstants.TILE_NUMBER[0]);
        float y = DrawHelper.random_number(-WindowConstants.TILE_NUMBER[1], WindowConstants.TILE_NUMBER[1]);
        while (DrawHelper.is_in(new float[]{x, y}, snake_positions)) {
            x = DrawHelper.random_number(-WindowConstants.TILE_NUMBER[0], WindowConstants.TILE_NUMBER[0]);
            y = DrawHelper.random_number(-WindowConstants.TILE_NUMBER[1], WindowConstants.TILE_NUMBER[1]);
        }
        return new float[]{x, y};

    }


    public void add_cube() {
        Square last_square = this.squares.get(this.squares.size() - 1);
        switch (last_square.getDirection()) {
            case UP: {
                Square square = new Square(last_square.getX(), last_square.getY() - 1, Direction.UP, this.color);
                this.squares.add(square);
                break;
            }
            case DOWN: {
                Square square = new Square(last_square.getX(), last_square.getY() + 1, Direction.DOWN, this.color);
                this.squares.add(square);
                break;
            }
            case LEFT: {
                Square square = new Square(last_square.getX() + 1, last_square.getY(), Direction.LEFT, this.color);
                this.squares.add(square);
                break;
            }
            case RIGHT: {
                Square square = new Square(last_square.getX() - 1, last_square.getY(), Direction.RIGHT, this.color);
                this.squares.add(square);
                break;
            }

        }
        Square last_square2 = this.squares.get(this.squares.size() - 1);
        last_square2.setDirection(last_square.getDirection());
        this.turns.add(this.squares.size() - 1, last_square.getDirection());


    }

    public void move(Direction direction) {
        this.direction = direction;
        List<Square> squareList = this.squares;
        this.turns.add(0, this.direction);
        for (int i = 0, squareListSize = squareList.size(); i < squareListSize; i++) {
            Square square = squareList.get(i);
            square.move(this.turns.get(i));
        }
        this.turns.remove(this.turns.size() - 1);
    }

    public boolean is_collision_with_self() {
        float[] head_position = this.head.getXY();
        List<Square> squareList = this.squares;
        for (int i = 1; i < squareList.size(); i++) {
            Square square = squareList.get(i);
            if (Arrays.hashCode(square.getXY()) == Arrays.hashCode(head_position)) {
                return true;
            }
        }
        return false;
    }


    public int[][] get_snake_game_map() {
        int[][] out = new int[WindowConstants.TILE_NUMBER[1] * 2 + 2][WindowConstants.TILE_NUMBER[0] * 2 + 2];
        int[][] snakeGameMap = this.snake_game_map;
        for (int i = 0; i < snakeGameMap.length; i++) {
            int[] line = snakeGameMap[i];
            System.arraycopy(line, 0, out[i], 0, line.length);
        }
        return out;


    }

    public void update(Direction direction) {
        update_snake_elements_position();
        fill_snake_game_map();
        int[] start_coordinates = {(int) this.head.getX() + WindowConstants.TILE_NUMBER[0],
                -(int) this.head.getY() + WindowConstants.TILE_NUMBER[1]};
//        DrawHelper.print_snake_game_map(this.snake_game_map);


        this.a_star_helper = new AStar(get_snake_game_map(), start_coordinates[0], start_coordinates[1], false);
        //        AStar.print_path(path, snake_game_map);
        this.path = this.a_star_helper.findPathTo(
                (int) this.food.getX() + WindowConstants.TILE_NUMBER[0],
                -(int) this.food.getY() + WindowConstants.TILE_NUMBER[1]);

        if (this.path != null) {
            float[] direction_delta;
            try {
                direction_delta = new float[]{this.path.get(1).x - WindowConstants.TILE_NUMBER[0] - this.head.getX(), -(this.path.get(1).y - WindowConstants.TILE_NUMBER[1]) - this.head.getY()};
            } catch (Exception e) {
                e.printStackTrace();
                direction_delta = new float[]{this.path.get(0).x - WindowConstants.TILE_NUMBER[0] - this.head.getX(), -(this.path.get(0).y - WindowConstants.TILE_NUMBER[1]) - this.head.getY()};
            }


            if (Arrays.hashCode(direction_delta) == direction_hash_codes[0]) {
                if (this.direction!=Direction.DOWN) {
                    move(Direction.UP);
                }
//                else{
//                    move(this.direction);
//                }
            }
            if (Arrays.hashCode(direction_delta) == direction_hash_codes[1]) {
                if (this.direction!=Direction.UP) {
                    move(Direction.DOWN);
                }
//                else{
//                    move(this.direction);
//                }
            }
            if (Arrays.hashCode(direction_delta) == direction_hash_codes[3]) {
                if (this.direction!=Direction.RIGHT) {
                    move(Direction.LEFT);
                }
//                else{
//                    move(this.direction);
//                }
            }
            if (Arrays.hashCode(direction_delta) == direction_hash_codes[2]) {
                if (this.direction!=Direction.LEFT) {
                    move(Direction.RIGHT);
                }
//                else{
//                    move(this.direction);
//                }
            }
        } else {
            move(direction);
        }

        if (Arrays.hashCode(this.food.getXY()) == Arrays.hashCode(this.head.getXY())) {
            float[] food_position = get_non_snake_random_position();
            this.food.setXY(food_position[0], food_position[1]);
            add_cube();
        }
        if (is_collision_with_self()) {
            reset();
        }
    }

    public void reset() {
        this.squares.clear();
        this.turns.clear();
        this.head = new Square(0, 0, Direction.RIGHT, this.color);
        this.squares.add(head);
        this.direction = Direction.RIGHT;
        float[] food_position = get_non_snake_random_position();
        this.food = new Food(food_position[0], food_position[1], new float[]{0.1f, 0.4f, 0.2f, 1});
        this.turns.add(this.direction);

    }

    public void draw_path() {
        List<AStar.Node> path = this.path;
        if (path != null) {
            float[] color = {0.5f, 0.5f, 0.5f, 0.5f};
            for (int i = 0, pathSize = path.size(); i < pathSize; i++) {
                AStar.Node node = path.get(i);
                if (i==0){
                    color= new float[]{1, 1, 1, 1};
                }
                else{
                    color= new float[]{0.5f,0.5f,0.5f,0.5f};
                }
                float[] quad_points = {
                        (node.x - WindowConstants.TILE_NUMBER[0]) * WindowConstants.TILE_SIZE_X,
                        -(node.y - WindowConstants.TILE_NUMBER[1]) * WindowConstants.TILE_SIZE_Y,
                        WindowConstants.TILE_SIZE_X,
                        WindowConstants.TILE_SIZE_Y};
                DrawHelper.draw_quad(quad_points, color);
            }
        }
    }


    public void draw() {
        for (Square square : this.squares) {
            square.draw();
        }
        this.food.draw();
        draw_path();

    }

}
