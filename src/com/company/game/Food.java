package com.company.game;

import com.company.constants.Direction;

public class Food extends Square {
    public Food(float x, float y, float[] color) {
        super(x, y, Direction.UP, color);
    }
}
