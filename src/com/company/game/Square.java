package com.company.game;

import com.company.DrawHelper;
import com.company.constants.Direction;
import com.company.constants.WindowConstants;

public class Square {


    private Direction direction;
    private float x;
    private float y;
    private final float[] color;

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public float getX() {
        return x;
    }

    public Direction getDirection() {
        return direction;
    }

    public float getY() {
        return y;
    }

    public float[] getColor() {
        return color;
    }

    public Square(float x, float y, Direction direction, float[] color) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.color = color;
    }

    public void move(Direction direction) {
        this.direction = direction;
        int[] direction_value = direction.getValue();
        this.x += direction_value[0];
        this.y += direction_value[1];
        if ((this.x >= WindowConstants.TILE_NUMBER[0])) {
            this.x = -WindowConstants.TILE_NUMBER[0];
        }
        if ((this.x < -WindowConstants.TILE_NUMBER[0])) {
            this.x = WindowConstants.TILE_NUMBER[0];
        }
        if ((this.y >= WindowConstants.TILE_NUMBER[1])) {
            this.y = -WindowConstants.TILE_NUMBER[1];
        }
        if ((this.y < -WindowConstants.TILE_NUMBER[1])) {
            this.y = WindowConstants.TILE_NUMBER[1];
        }


    }

    public void draw() {
        float[] quad_points = {
                this.x * com.company.constants.WindowConstants.TILE_SIZE_X,
                this.y * com.company.constants.WindowConstants.TILE_SIZE_Y,
                com.company.constants.WindowConstants.TILE_SIZE_X,
                com.company.constants.WindowConstants.TILE_SIZE_Y};
        DrawHelper.draw_quad(quad_points, this.color);
    }

    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float[] getXY() {
        return new float[]{this.x, this.y};
    }
}
