package com.company.constants;

public enum Direction {
    UP(0),
    DOWN(1),
    RIGHT(2),
    LEFT(3);


    private final int[] value;

    public int[][] getValues() {
        return values;
    }

    private final int[][] values = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    Direction(int value) {
        this.value = values[value];
    }

    public int[] getValue() {
        return value;
    }
}

