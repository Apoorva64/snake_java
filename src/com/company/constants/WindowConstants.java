package com.company.constants;

public class WindowConstants {
    public static int WINDOW_WIDTH = 1920/2;
    public static int WINDOW_HEIGHT = 1080/2;
    static int factor = 1;
    public static int[] TILE_NUMBER = {16 * factor, 9 * factor};
    public static int TILE_SIZE_PIXEL = WINDOW_WIDTH / (TILE_NUMBER[0]);
    public static float TILE_SIZE_X = (float) TILE_SIZE_PIXEL / (float) WINDOW_WIDTH;
    public static float TILE_SIZE_Y = (float) TILE_SIZE_PIXEL / (float) WINDOW_HEIGHT;
}
