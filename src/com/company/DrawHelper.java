package com.company;

import com.company.constants.WindowConstants;

import java.util.Arrays;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

public class DrawHelper {
    public static void draw_line(float[][] line_points) {
        glBegin(GL_LINES);
        glColor4f(1, 0, 0, 0);
        glVertex2f(line_points[0][0], line_points[0][1]);
        glVertex2f(line_points[1][0], line_points[1][1]);
        glEnd();
    }

    public static void draw_quad(float[] quad_points, float[] color) {
        glBegin(GL_QUADS);
        glColor4f(color[0], color[1], color[2], color[3]);
        glVertex2d(quad_points[0], quad_points[1]);
        glVertex2d(quad_points[0] + quad_points[2], quad_points[1]);
        glVertex2d(quad_points[0] + quad_points[2], quad_points[1] + quad_points[3]);
        glVertex2d(quad_points[0], quad_points[1] + quad_points[3]);
        glEnd();

    }

    public static void draw_grid() {
        for (float x = -1; x < 1; x += WindowConstants.TILE_SIZE_X) {
            float[][] line_points = {{x, 1}, {x, -1}};
            draw_line(line_points);
        }
        for (float y = -1; y < 1; y += WindowConstants.TILE_SIZE_Y) {
            float[][] line_points = {{1, y}, {-1, y}};
            draw_line(line_points);
        }
    }

    public static int random_number(int min, int max) {
        Random random = new Random();
        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }

    public static boolean is_in(float[] element, float[][] array) {
        for (float[] array_elem : array) {
            if (Arrays.hashCode(array_elem) == Arrays.hashCode(element)) {
                return true;
            }
        }
        return false;

    }

    public static void print_snake_game_map(int[][] game_map) {
        for (int[] line : game_map) {
            System.out.println(Arrays.toString(line));
        }

    }
}
