package com.company;

import com.company.constants.Direction;
import com.company.constants.WindowConstants;
import com.company.game.Snake;
import org.lwjgl.opengl.GL;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

public class MainLoop {
    private final long window;
    public HashMap<String, Direction> input_hash_map;
    Snake snake;

    public MainLoop() {
        this.window = initialize_lwjgl();
        float[] color = {0.1f, 1, 0.5f, 0};
        this.snake = new Snake(0, 0, color);
        this.input_hash_map = new HashMap<>();
        while (!glfwWindowShouldClose(window)) {
            update_window();
        }

    }


    private void update_window() {
        // move/physics
        glfwPollEvents();
        get_inputs();
        if ((this.input_hash_map.get("snake_direction") != null)) {
            this.snake.update(this.input_hash_map.get("snake_direction"));
        }

        // Drawing
        glClear(GL_COLOR_BUFFER_BIT);
        DrawHelper.draw_grid();
        this.snake.draw();
        glfwSwapBuffers(this.window);
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void get_inputs() {
        if (glfwGetKey(this.window, GLFW_KEY_W) == GLFW_TRUE) {
            this.input_hash_map.put("snake_direction", Direction.UP);
        } else if (glfwGetKey(this.window, GLFW_KEY_S) == GLFW_TRUE) {
            this.input_hash_map.put("snake_direction", Direction.DOWN);
        } else if (glfwGetKey(this.window, GLFW_KEY_D) == GLFW_TRUE) {
            this.input_hash_map.put("snake_direction", Direction.RIGHT);
        } else if (glfwGetKey(this.window, GLFW_KEY_A) == GLFW_TRUE) {
            this.input_hash_map.put("snake_direction", Direction.LEFT);
        } else if (glfwGetKey(this.window, GLFW_KEY_0) == GLFW_TRUE) {
            this.snake.add_cube();
        }
    }

    public static long initialize_lwjgl() {
        if (!glfwInit()) {
            System.err.println("glfw failed to initialise");
        } else {
            System.err.println("glfw initialised successfully");
        }
        long window = glfwCreateWindow(WindowConstants.WINDOW_WIDTH, WindowConstants.WINDOW_HEIGHT, "window", 0, 0);
        glfwShowWindow(window);
        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        return window;
    }


}